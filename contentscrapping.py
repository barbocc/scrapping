from selenium import webdriver
import justext

class ContentScrapping:

    """
    Данный класс позволяет парсить выдачу яндекса и гугла, а так анализировать извлекать тексты напрямую из url
    """
    user = ''
    api_key = ''
    yandex_url = ''
    google_url = ''
    query = ''
    driver = None
    urls = []
    yandex_urls = []
    google_urls = []

    #парсим страниц из поисковой выдачи
    pages = []

    pages_from_google = []
    pages_from_yandex = []

    def __init__(self, query='', pages=1):
        self.query = query
        self.pages = pages
        if self.query != '':
            self.driver = webdriver.Firefox()
            self.driver.set_page_load_timeout(60)
            self.__get_yandex_urls__()
            self.__get_google_urls__()
            self.driver.close()
            self.driver = None

    def __get_yandex_urls__(self):
        # yandex
        get_query = 'https://yandex.ru/search/xml?user=konstantinLitkevich&key=03.123611872:a36d1da3f3af6723543d7940d077aa99&query='+self.query
        for i in range(self.pages):
            try:
                self.driver.get(get_query + '&page=' + str(i))
            except:
                break
            else:
                elements = (self.driver.find_elements_by_tag_name('url'))
                for element in elements:
                    self.urls.append(element.text)

    def __get_google_urls__(self):
        get_query = 'https://www.google.ru/search?q='+self.query

        for i in range(self.pages):
            try:
                self.driver.get(get_query + '&start='+str((i+1)*10 - 10))
            except:
                break
            else:
                #elements = (self.driver.find_elements_by_tag_name('cite'))
                links = self.driver.find_elements_by_css_selector('div.rc>h3.r>a')
                for a in links:
                    #self.urls.append(element.text)
                    self.driver.execute_script("window.open('');")
                    page = a.get_attribute('href')
                    self.driver.switch_to.window(self.driver.window_handles[1])
                    try:
                        self.driver.get(page)
                    except:
                        self.driver.close()
                        self.driver.switch_to.window(self.driver.window_handles[0])
                    else:
                        self.google_urls.append(self.driver.current_url)
                        self.driver.close()
                        self.driver.switch_to.window(self.driver.window_handles[0])

    def get_pages_from_urls(self):
        all_pages = []
        position = 1
        for url in self.yandex_urls:
            content = self.get_paragraphs_by_url(url)
            all_pages.append({'url': url, 'soource': 'yandex', 'position': position, 'content': content})
            position += 1
        position = 1
        print(self.google_urls)
        for url in self.google_urls:
            content = self.get_paragraphs_by_url(url)
            all_pages.append({'url': url, 'soource': 'yandex', 'position': position, 'content': content})
            position += 1
        return all_pages

    def get_paragraphs_by_url(self, url):
        if self.driver == None:
            self.driver = webdriver.Firefox()
            self.driver.set_page_load_timeout(60)
            driver_defined = True
        try:
            self.driver.get(url)
        except:
            return
        else:
            page = self.driver.page_source
            paragraphs = justext.justext(page, justext.get_stoplist("Russian"))
            if driver_defined == True:
                self.driver.close()
                self.driver = None
            return paragraphs

    def get_yandex_urls(self, query, pages):
        self.query = query
        self.pages = pages
        self.driver = webdriver.Firefox()
        self.driver.set_page_load_timeout(60)
        self.__get_google_urls__()
        self.driver.close()
        self.driver = None

    def get_google_urls(self, query, pages):
        self.query = query
        self.pages = pages
        self.driver = webdriver.Firefox()
        self.driver.set_page_load_timeout(60)
        self.__get_google_urls__()
        self.driver.close()
        self.driver = None
        return self.urls

    def get_all_urls(self, query, pages):
        self.query = query
        self.pages = pages
        self.driver = webdriver.Firefox()
        self.driver.set_page_load_timeout(60)
        self.__get_yandex_urls__()
        self.__get_google_urls__()
        self.driver.close()
        self.driver = None

class ScrapedPage:
    url = ''
    paragraph_content = []
    source_code = ''
    yandex_position = None
    google_position = None

    def __init__(self, url, paragraph_content, source_code, yandex_position = None, google_position = None):
        self.url = url
        self.paragraph_content = paragraph_content
        self.source_code = source_code
        self.yandex_position = yandex_position
        self.google_position = google_position

#test = ContentScrapping('привет мир', 1)
#all_pages = test.get_pages_from_urls()
#print(all_pages)